﻿<%@ Page Title="Home Page" Language="VB" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Default.aspx.vb" Inherits="DesktopNotificationPOC_2._Default" %>

<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">
    <script src="https://code.jquery.com/jquery-2.1.4.min.js"></script>

    <script>
        var isNotificationOpen = false;

        // Notification Scripting
        $(document).ready(function () {
            $('<audio id="notifyAudio"><source src="/Sounds/mixkit-positive-interface-beep-221.wav" type="audio/wav"></audio>').appendTo('body');
            setInterval(myTimer, 5000);
        });

        function myTimer() {
            notifyMe();
        }

        function createNotification() {

            var options = {
                body: 'Click on this notification',
                icon: 'stupidcodes.com.png',
                dir: 'ltr'
            };

            var notification = new Notification("Hello from RedRover", options);
            isNotificationOpen = true;

            notification.onclick = function () {
                window.open("http://www.Google.com/");
                notification.close();
                isNotificationOpen = false;
            };
            notification.onclose = function () {
                isNotificationOpen = false;
            }
            $('#notifyAudio')[0].play();
        }

        function notifyMe() {
            if (isNotificationOpen) {
                return;
            }
            if (!("Notification" in window)) {
                alert("This browser does not support desktop notification");
                isNotificationOpen = true;
            }
            else if (Notification.permission === "granted") {

                createNotification();
            }
            else if (Notification.permission !== 'denied') {
                Notification.requestPermission(function (permission) {
                    if (!('permission' in Notification)) {
                        Notification.permission = permission;
                    }
                    if (permission === 'granted') {

                        createNotification();
                    }
                });
            }
        }
        // End Nofication Scripting
    </script>

    <table style="margin: 20px; text-align: center;">
        <tr>
            <td>
                
            </td>
        </tr>
    </table>
</asp:Content>
